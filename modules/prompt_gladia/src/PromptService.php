<?php

namespace Drupal\prompt_gladia;


/**
 * Class PromptService
 * @package Drupal\prompt_gladia\Services
 */
class PromptService {

  public function settings_fields(){
    return [
      'secret_key',
      'type_gladia',
    ];
  }

  public function required_override_settings(): bool {
    return TRUE;
  }

  public function settings_form($override_config = []){
    $config = \Drupal::service('prompt.service')->override_config('prompt_gladia.settings', $override_config);

    $form['type_gladia'] = [
      '#type' => 'select',
      '#title' => t('Gladia Type'),
      '#description' => t('The Gladia type which will generate the output.'),
      '#required' => true,
      '#options' => [
        'gpt3' => 'Text to text (Gpt3)',
        'summarization' => 'Text to text (Summaritzation)',
        'audio-transcription' => 'Audio to text (Audio Transcription)',
        'youtube-audio-transcription' => 'Audio to text (Youtube audio Transcription)',
      ],
      '#default_value' => !empty($config->get('type_gladia')) ? $config->get('type_gladia') : 'gpt3',
    ];

    return $form;
  }

  function api_prompt_request($prompt, $config, $debug=FALSE){
    if(empty($prompt)){
      return '';
    }

    $secret_key = $config->get('secret_key');
    $type = $config->get('type_gladia');

    $headers = [
      'accept: application/json',
      'x-gladia-key: '.$secret_key,
//      'Content-Type: multipart/form-data',
      'Content-Type: application/x-www-form-urlencoded',
    ];

    if($type === 'gpt3'){
      $url = 'https://api.gladia.io/text/text/gpt3/';
      $request_body = "text= $prompt";
    }
    elseif($type === 'summarization'){
      $url = 'https://api.gladia.io/text/text/summarization/';
      $request_body = [
        "max_length" => '512',
        "min_length" => '40',
        "source_language" => "eng",
        "text" => $prompt,
      ];
    }
    elseif($type === 'audio-transcription'){
      $url = 'https://api.gladia.io/audio/text/audio-transcription/?model=large-v2';
      $headers = [
        'accept: application/json',
        'x-gladia-key: '.$secret_key,
        'Content-Type: multipart/form-data'
      ];
      $request_body = [
        "audio_url" => $prompt,
        "language" => "english",
        "language_behaviour" => "automatic single language",
        'noise_reduction' => 'true',
        'output_format' => 'json',
      ];
    }
    elseif($type === 'youtube-audio-transcription'){
      $url = 'https://api.gladia.io/audio/text/youtube-audio-transcription/?model=large-v2';
      $headers = [
        'accept: application/json',
        'x-gladia-key: '.$secret_key,
        'Content-Type: multipart/form-data'
      ];
      $request_body = [
        "audio_url" => $prompt,
        "language" => "english",
        "language_behaviour" => "automatic single language",
        'noise_reduction' => 'true',
        'output_format' => 'json',
      ];
    }

    if(empty($url) || empty($request_body)){
      return '';
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $request_body,
      CURLOPT_HTTPHEADER => $headers,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      \Drupal::messenger()->addMessage(
        t(
          'Error: @error', [
            '@error' => (string)$err,
          ]
        )
      );
    }
    else {
      $response = json_decode($response);
      if(!empty($response->statusCode) && $response->statusCode != 200 && !empty($response->prediction)){
        \Drupal::messenger()->addMessage(
          t(
            'Error: @error', [
              '@error' => (string)$response->statusCode,
            ]
          ), 'error'
        );
      }
      elseif(!empty($response->prediction)){
        if($debug == TRUE){
          \Drupal::messenger()->addMessage(
            t('Result: @result',
              [
                '@result' => $response->prediction
              ]
            ), 'warning');
        }

        if(is_string($response->prediction)){
          return $response->prediction;
        }
        else{
          return (string) json_encode($response->prediction);
        }
      }
      else{
        foreach ($response as $key => $value){
          if(!empty($value) && is_string($value)){
            \Drupal::messenger()->addMessage(
              t(
                'Error: @error', [
                  '@error' => $value,
                ]
              ), 'error'
            );
          }
        }
      }
    }

    return NULL;

  }

}
