<?php

namespace Drupal\prompt_gpt3;


/**
 * Class PromptService
 * @package Drupal\prompt_gpt3\Services
 */
class PromptService {

  public function settings_fields(){
    return [
      'secret_key',
      'model',
      'temperature',
      'max_tokens',
      'top_p',
      'frequency_penalty',
      'presence_penalty',
      'best_of',
    ];
  }

  public function required_override_settings(): bool {
    return FALSE;
  }

  public function settings_form($override_config = []){
    $config = \Drupal::service('prompt.service')->override_config('prompt_gpt3.settings', $override_config);

    $form['model'] = [
      '#type' => 'select',
      '#title' => t('Model'),
      '#description' => t('The model which will generate the completion. Some models are suitable for natural language tasks, others specialize in code. <a target="_blank" href="https://beta.openai.com/docs/models/gpt-3">Learn More</a>'),
      '#required' => true,
      '#options' => [
        'text-davinci-003' => 'text-davinci-003',
        'text-davinci-002' => 'text-davinci-002',
        'text-davinci-001' => 'text-davinci-001',
        'text-curie-001' => 'text-curie-001',
        'text-babbage-001' => 'text-babbage-001',
        'text-ada-001' => 'text-ada-001',
      ],
      '#default_value' => !empty($config->get('model')) ? $config->get('model') : 'text-davinci-003',
    ];

    $form['temperature'] = [
      '#type' => 'number',
      '#max' => 1,
      '#min' => 0,
      '#step' => 0.01,
      '#title' => t('Temperature'),
      '#description' => t('Controls randomness: Lowering results in less random completions. As the temperature approaches zero, the model will become deterministic and repetitive.'),
      '#required' => true,
      '#default_value' => !is_null($config->get('temperature')) ? $config->get('temperature') : '0.7',
    ];

    $form['max_tokens'] = [
      '#type' => 'number',
      '#max' => 4000,
      '#min' => 1,
      '#step' => 1,
      '#title' => t('Maximum length'),
      '#description' => t('The maximum number of tokens to generate. Requests can use up to 2,048 or 4,000 tokens shared between prompt and completion. The exact limit varies by model. (One token is roughly 4 characters for normal English text)'),
      '#required' => true,
      '#default_value' => !is_null($config->get('max_tokens')) ? $config->get('max_tokens') : '256',
    ];

    $form['top_p'] = [
      '#type' => 'number',
      '#max' => 1,
      '#min' => 0,
      '#step' => 0.01,
      '#title' => t('Top P'),
      '#description' => t('Controls diversity via nucleus sampling: 0.5 means half of all likelihood-weighted options are considered.'),
      '#required' => true,
      '#default_value' => !is_null($config->get('top_p')) ? $config->get('top_p') : '1',
    ];

    $form['frequency_penalty'] = [
      '#type' => 'number',
      '#max' => 2,
      '#min' => 0,
      '#step' => 0.01,
      '#title' => t('Frequency penalty'),
      '#description' => t("How much to penalize new tokens based on their existing frequency in the text so far. Decreases the model's likelihood to repeat the same line verbatim."),
      '#required' => true,
      '#default_value' => !is_null($config->get('frequency_penalty')) ? $config->get('frequency_penalty') : '0',
    ];

    $form['presence_penalty'] = [
      '#type' => 'number',
      '#max' => 2,
      '#min' => 0,
      '#step' => 0.01,
      '#title' => t('Presence penalty'),
      '#description' => t("How much to penalize new tokens based on whether they appear in the text so far. Increases the model's likelihood to talk about new topics."),
      '#required' => true,
      '#default_value' => !is_null($config->get('presence_penalty')) ? $config->get('presence_penalty') : '0',
    ];

    $form['best_of'] = [
      '#type' => 'number',
      '#max' => 20,
      '#min' => 1,
      '#step' => 1,
      '#title' => t('Best of'),
      '#description' => t("Generates multiple completions server-side, and displays only the best. Streaming only works when set to 1. Since it acts as a multiplier on the number of completions, this parameters can eat into your token quota very quickly – use caution!"),
      '#required' => true,
      '#default_value' => !is_null($config->get('best_of')) ? $config->get('best_of') : '1',
    ];

    return $form;
  }

  function api_prompt_request($prompt, $config, $debug=FALSE){
    if(empty($prompt)){
      return '';
    }

    $secret_key = $config->get('secret_key');
    $model = $config->get('model');
    $temperature = $config->get('temperature');
    $max_tokens = $config->get('max_tokens');
    $top_p = $config->get('top_p');
    $frequency_penalty = $config->get('frequency_penalty');
    $presence_penalty = $config->get('presence_penalty');
    $best_of = $config->get('best_of');
    $request_body = [
      "prompt" => $prompt,
      "model" => $model,
      "max_tokens" => (int)$max_tokens,
      "temperature" => (float)$temperature,
      "top_p" => (float)$top_p,
      "presence_penalty" => (float)$presence_penalty,
      "frequency_penalty"=> (float)$frequency_penalty,
      "best_of"=> (int)$best_of,
      "stream" => false,
    ];

    $postfields = json_encode($request_body);
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.openai.com/v1/completions",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 60,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postfields,
      CURLOPT_HTTPHEADER => [
        "Content-Type: application/json",
        "Authorization: Bearer $secret_key",
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      \Drupal::messenger()->addMessage(
        t(
          'Error: @error', [
            '@error' => (string)$err,
          ]
        )
      );
    }
    else {
      $response = json_decode($response);

      if($debug == TRUE && !empty($response->usage)){
        \Drupal::messenger()->addMessage(
          t(
            'prompt_tokens:[@prompt_tokens], completion_tokens:[@completion_tokens]  -> total_tokens:[@total_tokens]', [
              '@prompt_tokens' => !empty($response->usage->prompt_tokens) ? $response->usage->prompt_tokens : 0,
              "@completion_tokens" => !empty($response->usage->completion_tokens) ? $response->usage->completion_tokens : 0,
              "@total_tokens" => !empty($response->usage->total_tokens) ? $response->usage->total_tokens : 0,
            ]
          ), 'warning');
      }

      if(!empty($response->error)){
        \Drupal::messenger()->addMessage(
          t(
            'Error: @error', [
              '@error' => (string)$response->error->message,
            ]
          ), 'error'
        );
      }
      else{
        return (string) $response->choices[0]->text;
      }
    }

    return NULL;

  }

}
