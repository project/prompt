<?php

/**
 * @file
 * Contains Drupal\prompt_gpt3\Form\MessagesForm.
 */

namespace Drupal\prompt_gpt3\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'prompt_gpt3.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gpt3_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prompt_gpt3.settings');

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('The OpenAi api key.'),
      '#required' => true,
      '#default_value' => $config->get('secret_key'),
    ];

    $form['gpt3_fieldset'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => t('GPT3 Settings'),
    ];

    $form['gpt3_fieldset'] += \Drupal::service('gpt3.prompt_service')->settings_form();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $fields = \Drupal::service('gpt3.prompt_service')->settings_fields();
    $config = $this->config('prompt_gpt3.settings');
    foreach ($fields as $key => $field_name){
      $config->set($field_name, $form_state->getValue($field_name));
    }
    $config->save();
  }

}
