<?php

namespace Drupal\prompt;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityViewBuilder for Prompt entities.
 */
class PromptEntityViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $handler = parent::createInstance($container, $entity_type);
    return $handler;
  }

  /**
   * {@inheritdoc}
   */
  public function viewMultiple(array $entities = [], $view_mode = 'full', $langcode = NULL) {
    /** @var \Drupal\prompt\Entity\PromptEntityInterface[] $entities */
    $build = [];

    foreach ($entities as $entity_id => $entity) {
      //generate form
      $form = \Drupal::formBuilder()->getForm('Drupal\prompt\Form\PromptTest', $entity->id());
      $build[$entity_id] = $form;
    }

    return $build;
  }

}
