<?php

namespace Drupal\prompt\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Prompt setting entities.
 */
interface PromptEntityInterface extends ConfigEntityInterface {

}
