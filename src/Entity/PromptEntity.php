<?php

namespace Drupal\prompt\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Prompt setting entity.
 *
 * @ConfigEntityType(
 *   id = "prompt",
 *   label = @Translation("Prompt setting"),
 *   handlers = {
 *     "view_builder" = "Drupal\prompt\PromptEntityViewBuilder",
 *     "list_builder" = "Drupal\prompt\PromptEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\prompt\Form\PromptEntityForm",
 *       "edit" = "Drupal\prompt\Form\PromptEntityForm",
 *       "delete" = "Drupal\prompt\Form\PromptEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\prompt\PromptEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "prompt",
 *   admin_permission = "administer prompt configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/prompt/{prompt}",
 *     "add-form" = "/admin/config/system/prompt/add",
 *     "edit-form" = "/admin/config/system/prompt/{prompt}/edit",
 *     "delete-form" = "/admin/config/system/prompt/{prompt}/delete",
 *     "enable" = "/admin/config/system/prompt/{prompt}/enable",
 *     "disable" = "/admin/config/system/prompt/{prompt}/disable",
 *     "collection" = "/admin/config/system/prompt"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "weight",
 *     "status",
 *     "prompt_text",
 *     "field_values_text",
 *     "join_chunks_prompt_text",
 *     "max_lenght",
 *     "max_number_times",
 *     "join_chunks",
 *     "type",
 *     "generate_chunks",
 *     "override_config",
 *   }
 * )
 */
class PromptEntity extends ConfigEntityBase implements PromptEntityInterface {

  /**
   * The Prompt setting ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Prompt setting label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Prompt setting description.
   *
   * @var string
   */
  protected $description = '';

  /**
   * The weight for sorting prompts.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The status, whether to be used by default.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * The Prompt setting text input.
   *
   * @var string
   */
  protected $prompt_text = '';

  /**
   * The Prompt field value text input.
   *
   * @var string
   */
  protected $field_values_text = '';

  /**
   * The Prompt join chunks value text input.
   *
   * @var string
   */
  protected $join_chunks_prompt_text = '';

  /**
   * The max lenght of characters for the chunks.
   *
   * @var int
   */
  protected $max_lenght = 0;

  /**
   * The max number of times to analize the chunks.
   *
   * @var int
   */
  protected $max_number_times = 0;

  /**
   * The Type setting text input.
   *
   * @var string
   */
  protected $type = '';

  /**
   * The generate_chunks setting, whether to be used by default.
   *
   * @var bool
   */
  protected $generate_chunks = FALSE;

  /**
   * The join_chunks setting, whether to be used by default.
   *
   * @var bool
   */
  protected $join_chunks = FALSE;

  /**
   * The override_config setting text input.
   *
   * @var string
   */
  protected $override_config = '';
}
