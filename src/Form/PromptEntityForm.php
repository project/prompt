<?php

namespace Drupal\prompt\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entity form.
 */
class PromptEntityForm extends EntityForm {

  /**
   * The drupal state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Extension\ThemeHandler definition.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The drupal state.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   */
  public function __construct(StateInterface $state, ThemeHandlerInterface $themeHandler) {
    $this->state = $state;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\prompt\Entity\PromptEntityInterface $config */
    $config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Label for the Prompt setting."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\prompt\Entity\PromptEntity::load',
      ],
    ];

    $form['static_fieldset'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('General Settings'),
    ];
    $form['static_fieldset']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Describe this Prompt setting. The text will be displayed on the <em>Prompt setting</em> list page.'),
      '#default_value' => $config->get('description'),
    ];
    $form['static_fieldset']['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#description' => $this->t('The weight to order the prompts.'),
      '#default_value' => $config->get('weight'),
    ];
    $form['static_fieldset']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('Active Prompt get used by default, this property can be overwritten like any other config entity in settings.php.'),
      '#default_value' => ($config->get('status') ? TRUE : FALSE),
    ];

    $form['input_fieldset'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Input Values Settings'),
    ];
    $token_types = ['user', 'node', 'term'];
    $form['input_fieldset']['prompt_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt Text'),
      '#description' => $this->t('Prompt input value. Drupal tokens has permitted.'),
      '#default_value' => $config->get('prompt_text'),
      '#element_validate' => ['token_element_validate'],
      '#after_build' => ['token_element_validate'],
      '#token_types' => $token_types,
      '#min_tokens' => 0,
    ];
    $form['input_fieldset']['field_values_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field Value Text'),
      '#description' => $this->t('Field Values to concatenate after Prompt text. Drupal tokens has permitted.'),
      '#default_value' => $config->get('field_values_text'),
      '#element_validate' => ['token_element_validate'],
      '#after_build' => ['token_element_validate'],
      '#token_types' => $token_types,
      '#min_tokens' => 0,
    ];
    // Show the token help
    $form['input_fieldset']['pattern_container']['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    ];

    $form['chunk_fieldset'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Chunk Settings'),
    ];
    $form['chunk_fieldset']['generate_chunks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate Chunks'),
      '#description' => $this->t('Allows to generate chunks of original field value text.'),
      '#default_value' => ($config->get('generate_chunks') ? TRUE : FALSE),
    ];
    $form['chunk_fieldset']['max_lenght'] = [
    '#type' => 'number',
    '#max' => 4000,
    '#min' => 1,
    '#step' => 1,
    '#title' => t('Maximum length'),
    '#description' => t('The maximum number of characters to analise of field values.'),
    '#required' => true,
    '#default_value' => !empty($config->get('max_lenght')) ? $config->get('max_lenght') : 1000,
    '#states' => [
      'visible' => [
        ':input[name="generate_chunks"]' => ['checked' => TRUE],
      ],
    ],
  ];
    $form['chunk_fieldset']['max_number_times'] = [
      '#type' => 'select',
      '#title' => t('Maximum number of times'),
      '#description' => t('The maximum number of times to call the API. 1 time = only the first chunk is analyzed..'),
      '#required' => true,
      '#options' => [
        0 => t('Unlimited'),
        1 => t('1 time'),
        2 => t('2 times'),
        3 => t('3 times'),
        4 => t('4 times'),
        5 => t('5 times'),
        6 => t('6 times'),
        7 => t('7 times'),
        8 => t('8 times'),
        9 => t('9 times'),
        10 => t('10 times'),
      ],
      '#default_value' => !empty($config->get('max_number_times')) ? $config->get('max_number_times') : 0,
      '#states' => [
        'visible' => [
          ':input[name="generate_chunks"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['chunk_fieldset']['join_chunks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Join Chunks'),
      '#description' => $this->t('Allow to join the resulting pieces using another prompt.'),
      '#default_value' => ($config->get('join_chunks') ? TRUE : FALSE),
      '#states' => [
        'visible' => [
          ':input[name="generate_chunks"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['chunk_fieldset']['join_chunks_prompt_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt Text for Join the Chunks'),
      '#description' => $this->t('Prompt input value. Drupal tokens has permitted.'),
      '#default_value' => $config->get('join_chunks_prompt_text'),
      '#element_validate' => ['token_element_validate'],
      '#after_build' => ['token_element_validate'],
      '#token_types' => $token_types,
      '#min_tokens' => 0,
      '#states' => [
        'visible' => [
          ':input[name="generate_chunks"]' => ['checked' => TRUE],
          ':input[name="join_chunks"]' => ['checked' => TRUE],
        ],
      ],
    ];


    $moduleHandler = \Drupal::service('module_handler');
    $type_options = [];
    if ($moduleHandler->moduleExists('prompt_gpt3')) {
      $type_options['gpt3'] = 'GPT3';
    }
    if ($moduleHandler->moduleExists('prompt_gladia')) {
      $type_options['gladia'] = 'Gladia';
    }
    $form['type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#description' => t('Select the type of artificial intelligence you want to use.'),
      '#required' => true,
      '#options' => $type_options,
      '#default_value' => !empty($config->get('type')) ? $config->get('type') : '',
    ];

    $form['override_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override Settings'),
      '#description' => $this->t('Allows to overwrite the default settings.'),
      '#default_value' => ($config->get('override_config') ? TRUE : FALSE),
    ];
    $form['override_config_fieldset'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Override Settings'),
      '#states' => [
        'visible' => [
          ':input[name="override_config"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $type = $config->get('type');
    if(!empty(\Drupal::hasService($type.'.prompt_service'))){
      $required_override = \Drupal::service($type.'.prompt_service')->required_override_settings();
      if($required_override){
        $form['override_config']['#default_value'] = TRUE;
        $form['override_config']['#disabled'] = TRUE;
        $form['override_config_fieldset']['#open'] = TRUE;
      }

      $override_config = json_decode($config->get('override_config'));
      $form['override_config_fieldset'] += \Drupal::service($type.'.prompt_service')->settings_form($override_config);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $prompt = $this->entity;

    //generate json for overrided settings
    if($prompt->get('override_config')) {
      $type = $prompt->get('type');
      $override_fields = \Drupal::service($type.'.prompt_service')->settings_fields();
      $override_config = [];
      foreach ($override_fields as $key => $field_name) {
        $value = $prompt->get($field_name);
        if (isset($value)) {
          $override_config[$field_name] = $value;
        }
      }
      $override_config = json_encode($override_config);
      $prompt->set('override_config', $override_config);
    }

    $status = $prompt->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Prompt setting.', [
          '%label' => $prompt->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Prompt setting.', [
          '%label' => $prompt->label(),
        ]));
    }
    $form_state->setRedirectUrl($prompt->toUrl('collection'));

    return $status;
  }

}
