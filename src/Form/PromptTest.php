<?php
/**
 * @file
 * Contains \Drupal\prompt\Form\PromptTest.
 */
namespace Drupal\prompt\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PromptTest extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prompt_test_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $prompt_id = NULL) {
    $form = [];

    if(empty($prompt_id)){
      return [];
    }

    $entity = \Drupal::entityTypeManager()->getStorage('prompt')->load($prompt_id);

    if($entity){
      $form['original_prompt_title'] = [
        '#prefix' => '<h5>',
        '#suffix' => '</h5>',
        '#markup' => $this->t('Original Prompt').':',
      ];
      $form['original_prompt'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $entity->get('prompt_text'),
      ];
      $form['original_field_values'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $entity->get('field_values_text'),
      ];

      $form['entity_type'] = array(
        '#type' => 'select',
        '#title' => t('Entity Tye:'),
        '#required' => FALSE,
        '#options' => [
          '0' => $this->t('None'),
          'node' => $this->t('Node'),
          'taxonomy_term' => $this->t('Taxonomy Term'),
          'user' => $this->t('User'),
        ],
        '#default_value' => $entity->get('entity_type'),
      );

      $form['entity_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Entity ID:'),
        '#required' => FALSE,
        '#default_value' => $entity->get('entity_id'),
      );

      if ($form_state->getTriggeringElement()) {
        $prompt = $form_state->getValue('prompt');
        $generated_text = $form_state->getValue('generated_text');
        $form['results']['prompt_title'] = [
          '#prefix' => '<h5>',
          '#suffix' => '</h5>',
          '#markup' => $this->t('Prompt').':',
        ];
        $form['results']['prompt'] = [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#markup' => $prompt,
        ];
        $form['results']['generated_text_title'] = [
          '#prefix' => '<h5>',
          '#suffix' => '</h5>',
          '#markup' => $this->t('Results').':',
        ];
        $form['results']['generated_text'] = [
          '#prefix' => '<p>',
          '#suffix' => '</p>',
          '#markup' => $generated_text,
        ];
      }


      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Use Prompt'),
        '#button_type' => 'primary',
      );
    }
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);

    //get prompt_id
    $build_info = $form_state->getBuildInfo();
    $prompt_id = $build_info['args'][0];

    //load entity for get the fields values
    $entity = NULL;
    if(!empty($form_state->getValue('entity_id')) && !empty($form_state->getValue('entity_type'))) {
      $entity = \Drupal::entityTypeManager()->getStorage($form_state->getValue('entity_type'))->load($form_state->getValue('entity_id'));
    }

    //get tokenized prompt text
    /** @var \Drupal\prompt\Entity\PromptEntityInterface $config */
    $config = \Drupal::config('prompt.prompt.' . $prompt_id);
    if(empty($config)){
      return '';
    }

    $prompt = $config->get('prompt_text');
    $field_values_text = $config->get('field_values_text');
    if(!empty($entity)){
      $language_interface = \Drupal::languageManager()->getCurrentLanguage();
      $entity_type = $entity->getEntityType()->id();
      $prompt = \Drupal::token()->replace($prompt, [$entity_type => $entity], ['langcode' => $language_interface->getId()]);
      $field_values_text = \Drupal::token()->replace($field_values_text, [$entity_type => $entity], ['langcode' => $language_interface->getId()]);
    }
    $prompt = implode(' ', [$prompt, $field_values_text]);
    $form_state->setValue('prompt', $prompt);

    $result = \Drupal::service('prompt.service')->prompt_request($prompt_id, $entity, TRUE);
    $form_state->setValue('generated_text', $result);
  }

}
