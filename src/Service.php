<?php

namespace Drupal\prompt;

/**
 * Class Service
 * @package Drupal\prompt\Services
 */
class Service {

  public function override_config($config_name, $override_config = [], $original_config = []){
    $config = \Drupal::configFactory()->getEditable($config_name);
    if(!empty($original_config)){
      $original_config = $original_config->getRawData();
      foreach ($original_config as $config_field_name => $config_field_value){
        $config->set($config_field_name, $config_field_value);
      }
    }

    if(!empty($override_config)){
      foreach ($override_config as $config_field_name => $config_field_value){
        $config->set($config_field_name, $config_field_value);
      }
    }

    return $config;
  }

  public function prompt_chunk_text($prompt, $text, $max_lenght, $max_number_times) {
    $prompt = trim($prompt);
    $text = trim($text);
    $text_array = explode(". ", $text);
    $result = [];
    $chunk = "";

    foreach ($text_array as $sentence) {
      if (strlen($chunk . $sentence) > $max_lenght) {
        $result[] = implode(' ', [$prompt, $chunk]);
        $chunk = "";
        if(count($result) >= $max_number_times && $max_number_times != 0){
          return $result;
        }
      }
      $chunk .= $sentence . ". ";
    }
    if ($chunk) {
      $chunk = substr($chunk, 0, -1);
      $result[] = implode(' ', [$prompt, $chunk]);
    }

    return $result;
  }

  public function prompt_request($prompt_id, $entity = NULL, $debug=FALSE){

    /** @var \Drupal\prompt\Entity\PromptEntityInterface $config */
    $config = \Drupal::config('prompt.prompt.' . $prompt_id);
    if(empty($config)){
      return '';
    }

    $chunks = $this->prepare_prompt_text($config, $entity);

    $override_config = json_decode($config->get('override_config'));
    $config_type = $config->get('type');
    $config = $this->override_config('prompt_'.$config_type.'.settings', $override_config, $config);

    $result_chunk = [];
    foreach ($chunks as $chunk_prompt){
      $result_chunk[] = \Drupal::service($config_type.'.prompt_service')->api_prompt_request($chunk_prompt, $config, $debug);
    }

    if(count($result_chunk) > 1){
      $join_chunks = $config->get('join_chunks');
      $join_chunks_prompt_text = $config->get('join_chunks_prompt_text');
      if($join_chunks === TRUE && !empty($join_chunks_prompt_text)){
        $prompt = implode("\n", $result_chunk);
        $prompt = implode("\n", [$join_chunks_prompt_text, $prompt]);
        $result = \Drupal::service($config_type.'.prompt_service')->api_prompt_request($prompt, $config, $debug);
      }
      else{
        $result = implode("\n", $result_chunk);
      }
    }
    else{
      $result = $result_chunk[0];
    }

    if(!empty($result)){
      $result = trim($result);
      $result = nl2br($result);
    }
    else{
      $result = '';
    }
    return $result;
  }

  public function prepare_prompt_text($config, $entity){
    $prompt = $config->get('prompt_text');
    $field_values = $config->get('field_values_text');

    //get tokenized prompt text
    $language_interface = \Drupal::languageManager()->getCurrentLanguage();
    if(!empty($entity)){
      $entity_type = $entity->getEntityType()->id();
      $prompt = \Drupal::token()->replace($prompt, [$entity_type => $entity], ['langcode' => $language_interface->getId()]);
      $field_values = \Drupal::token()->replace($field_values, [$entity_type => $entity], ['langcode' => $language_interface->getId()]);
    }
    else{
      $prompt = \Drupal::token()->replace($prompt, ['langcode' => $language_interface->getId()]);
      $field_values = \Drupal::token()->replace($field_values, ['langcode' => $language_interface->getId()]);
    }

    //TODO make optional striptags
    $prompt = strip_tags($prompt);
    $field_values = strip_tags($field_values);

    $max_lenght = $config->get('max_lenght');
    $max_number_times = $config->get('max_number_times');
    $generate_chunks = $config->get('generate_chunks');

    if($generate_chunks === TRUE){
      $chunks = $this->prompt_chunk_text($prompt, $field_values, $max_lenght, $max_number_times);
    }
    else{
      $chunks[0] = implode(' ', [$prompt, $field_values]);
    }

    return $chunks;
  }

}
