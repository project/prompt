<?php

namespace Drupal\prompt\Plugin\Action;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\ListInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\eca\Plugin\DataType\DataTransferObject;
use Drupal\eca_content\Plugin\Action\EcaFieldUpdateActionInterface;
use Drupal\eca_content\Plugin\Action\FieldUpdateActionBase;
use Drupal\eca_content\Plugin\Action\SetFieldValue;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Set the value of an entity field.
 *
 * @Action(
 *   id = "prompt_set_field_value",
 *   label = @Translation("Prompt: set field value with IA"),
 *   description = @Translation("Allows to set, unset or change the value(s) of any field in an entity."),
 *   type = "entity"
 * )
 */
class PromptSetFieldValue extends SetFieldValue {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'prompt_id' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['prompt_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prompt Id'),
      '#description' => $this->t('The Prompt Id to use. This property supports tokens.'),
      '#default_value' => $this->configuration['prompt_id'],
      '#weight' => -10,
    ];
    unset($form['field_value']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['prompt_id'] = $form_state->getValue('prompt_id');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsToUpdate() {
    $name = $this->tokenServices->replace($this->configuration['field_name']);
    return [$name => ''];
  }

  /**
   * {@inheritdoc}
   */
  protected function getValueToUpdate($entity) {
    $name = $this->tokenServices->replace($this->configuration['field_name']);
    $prompt_id = $this->configuration['prompt_id'];

    $values = \Drupal::service('prompt.service')->prompt_request($prompt_id, $entity, TRUE);

    // Process the field values.
    $use_token_replace = TRUE;
    // Check whether the input wants to directly use defined data.
    if ((mb_substr($values, 0, 1) === '[') && (mb_substr($values, -1, 1) === ']') && (mb_strlen($values) <= 255) && ($data = $this->tokenServices->getTokenData($values))) {
      if (!($data instanceof TypedDataInterface) || !empty($data->getValue())) {
        $use_token_replace = FALSE;
        $values = $data;
      }
    }
    if ($use_token_replace) {
      $values = $this->tokenServices->replaceClear($values);
    }

    return [$name => $values];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function execute($entity = NULL) {
    if (!($entity instanceof FieldableEntityInterface)) {
      return;
    }

    $method_settings = explode(':', $this->configuration['method'] ?? ($this->defaultConfiguration()['method'] ?? 'set:clear'));
    $all_entities_to_save = [];
    $options = ['auto_append' => TRUE, 'access' => 'update'];
    $values_changed = FALSE;

    foreach ($this->getValueToUpdate($entity) as $field => $values) {
      $metadata = [];
      if (!($update_target = $this->getTypedProperty($entity->getTypedData(), $field, $options, $metadata))) {
        throw new \InvalidArgumentException(sprintf("The provided field %s does not exist as a property path on the %s entity having ID %s.", $field, $entity->getEntityTypeId(), $entity->id()));
      }
      if (empty($metadata['entities'])) {
        throw new \RuntimeException(sprintf("The provided field %s does not resolve for entities to be saved from the %s entity having ID %s.", $field, $entity->getEntityTypeId(), $entity->id()));
      }
      $property_name = $update_target->getName();
      $delta = 0;
      while ($update_target = $update_target->getParent()) {
        if (is_int($update_target->getName())) {
          $delta = $update_target->getName();
        }
        if ($update_target instanceof FieldItemListInterface) {
          break;
        }
      }
      $is_property_name_explicit = in_array($property_name, $metadata['parts']);
      $is_delta_explicit = in_array((string) $delta, $metadata['parts']);
      if (!($update_target instanceof FieldItemListInterface)) {
        throw new \InvalidArgumentException(sprintf("The provided field %s does not resolve to a field on the %s entity having ID %s.", $field, $entity->getEntityTypeId(), $entity->id()));
      }
      if ($values instanceof ListInterface) {
        $values = $values->getValue();
      }
      elseif ($values instanceof DataTransferObject) {
        if ($properties = $values->getProperties()) {
          $values = [];
          foreach ($properties as $k => $v) {
            $values[$k] = $v instanceof DataTransferObject ? $v->toArray() : $v->getValue();
          }
        }
        else {
          $values = [$delta => $values->getString()];
        }
      }
      elseif (!is_array($values)) {
        $values = [$delta => $values];
      }
      if (!isset($values[$delta])) {
        $values[$delta] = end($values);
        unset($values[key($values)]);
      }

      // Apply configured filters and normalize the array of values.
      foreach ($values as $i => $value) {
        if ($value instanceof TypedDataInterface) {
          $value = $value->getValue();
          $values[$i] = $value;
        }
        if (is_array($value) && ($is_property_name_explicit || (count($value) === 1))) {
          $value = array_key_exists($property_name, $value) ? $value[$property_name] : reset($value);
        }
        if (is_scalar($value) || is_null($value)) {
          if (!empty($this->configuration['strip_tags'])) {
            $value = preg_replace('/[\t\n\r\0\x0B]/', '', strip_tags((string) $value));
          }
          if (!empty($this->configuration['trim'])) {
            $value = trim((string) $value);
          }
          if ($value === '' || $value === NULL) {
            unset($values[$i]);
          }
          else {
            $values[$i] = [$property_name => $value];
          }
        }
      }

      // Custom filtering of field values is applied here, because some fields
      // do actually want to have an incomplete instermediary state of a field
      // value, that would be then completed by a subsequent action. Therefore
      // a manual filter is performed here.
      /** @var \Drupal\Core\Field\FieldItemListInterface $update_target */
      $current_values = array_filter($update_target->getValue(), function ($value) {
        if (is_array($value)) {
          foreach ($value as $v) {
            if (!is_null($v)) {
              return TRUE;
            }
          }
          return FALSE;
        }
        return !is_null($value) && ($value !== '');
      });

      if ($is_delta_explicit) {
        $values += $current_values;
        ksort($values);
      }

      if (empty($values) && !empty($current_values) && (($this->configuration['method'] ?? 'set:clear') === 'set:clear')) {
        // Shorthand for setting a field to be empty.
        if ($is_property_name_explicit) {
          $update_target->get($delta)->$property_name = NULL;
        }
        else {
          $update_target->setValue([]);
        }
        foreach ($metadata['entities'] as $entity_to_save) {
          if (!in_array($entity_to_save, $all_entities_to_save, TRUE)) {
            $all_entities_to_save[] = $entity_to_save;
          }
        }
        continue;
      }

      // Create a map of indices that refer to the already existing counterpart.
      $existing = [];
      foreach ($current_values as $k => $current_item) {
        if (($i = array_search($current_item, $values)) !== FALSE) {
          $existing[$i] = $k;
          continue;
        }

        $current_value = !is_array($current_item) ? $current_item : (array_key_exists($property_name, $current_item) ? $current_item[$property_name] : reset($current_item));
        if (is_string($current_value)) {
          // Extra processing is needed for strings, in order to prevent false
          // comparison when dealing with values that are the same but
          // encoded differently.
          $current_value = nl2br(trim($current_value));
        }

        foreach ($values as $i => $value) {
          $new_value = !is_array($value) ? $value : (array_key_exists($property_name, $value) ? $value[$property_name] : reset($value));
          if (is_string($new_value)) {
            $new_value = nl2br(trim($new_value));
          }
          if (((is_object($new_value) && $current_value === $new_value) || ($current_value == $new_value)) && !isset($existing[$i]) && !in_array($k, $existing, TRUE)) {
            $existing[$i] = $k;
          }
          if (($i === $k) && is_array($value) && is_array($current_item) && (reset($method_settings) === 'set')) {
            $values[$i] += $current_item;
          }
        }
      }

      if ((reset($method_settings) !== 'remove') && (count($existing) === count($values)) && (count($existing) === count($current_values))) {
        continue;
      }

      $cardinality = $update_target->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
      $is_unlimited = $cardinality === FieldStorageConfigInterface::CARDINALITY_UNLIMITED;
      foreach ($method_settings as $method_setting) {
        switch ($method_setting) {

          case 'clear':
            $keep = [];
            foreach ($existing as $k) {
              $keep[$k] = $current_values[$k];
            }
            if (count($current_values) !== count($keep)) {
              $values_changed = TRUE;
            }
            $current_values = $keep;
            break;

          case 'empty':
            if (empty($current_values)) {
              break;
            }
            if ($is_delta_explicit && empty($current_values[$delta])) {
              break;
            }
            if ($is_property_name_explicit && empty($current_values[$delta][$property_name])) {
              break;
            }
            continue 3;

          case 'not_full':
            if (!$is_unlimited && !(count($current_values) < $cardinality)) {
              continue 3;
            }
            break;

          case 'drop_first':
            if (!$is_unlimited) {
              $num_required = count($values) - count($existing) - ($cardinality - count($current_values));
              $keep = array_flip($existing);
              reset($current_values);
              while ($num_required > 0 && ($k = key($current_values)) !== NULL) {
                next($current_values);
                $num_required--;
                if (!isset($keep[$k])) {
                  unset($current_values[$k]);
                  $values_changed = TRUE;
                }
              }
            }
            break;

          case 'drop_last':
            if (!$is_unlimited) {
              $num_required = count($values) - count($existing) - ($cardinality - count($current_values));
              $keep = array_flip($existing);
              end($current_values);
              while ($num_required > 0 && ($k = key($current_values)) !== NULL) {
                prev($current_values);
                $num_required--;
                if (!isset($keep[$k])) {
                  unset($current_values[$k]);
                  $values_changed = TRUE;
                }
              }
            }
            break;

        }
      }

      foreach ($method_settings as $method_setting) {
        switch ($method_setting) {

          case 'set':
            $current_num = count($current_values);
            foreach ($values as $i => $value) {
              if (($is_delta_explicit || ($is_property_name_explicit && ($delta === 0) && ($i === 0))) && !isset($existing[$i])) {
                $current_values[$i] = $value;
                $values_changed = TRUE;
                continue;
              }
              if (!$is_unlimited && $cardinality <= $current_num) {
                break;
              }
              if (!isset($existing[$i])) {
                $current_num++;
                $current_values[] = $value;
                $values_changed = TRUE;
              }
            }
            ksort($current_values);
            break;

          case 'append':
            $current_num = count($current_values);
            foreach ($values as $i => $value) {
              if (!$is_unlimited && $cardinality <= $current_num) {
                break;
              }
              if (!isset($existing[$i])) {
                array_push($current_values, $value);
                $current_num++;
                $values_changed = TRUE;
              }
            }
            break;

          case 'prepend':
            $current_num = count($current_values);
            foreach (array_reverse($values, TRUE) as $i => $value) {
              if (!$is_unlimited && $cardinality <= $current_num) {
                break;
              }
              if (!isset($existing[$i])) {
                array_unshift($current_values, $value);
                $current_num++;
                $values_changed = TRUE;
              }
            }
            break;

          case 'remove':
            foreach ($existing as $k) {
              unset($current_values[$k]);
              $values_changed = TRUE;
            }
            break;

        }
      }

      if ($values_changed) {
        // Try to set the values. If that attempt fails, then it would throw an
        // exception, and the exception would be logged as an error.
        $update_target->setValue(array_values($current_values));
        foreach ($metadata['entities'] as $entity_to_save) {
          if (!in_array($entity_to_save, $all_entities_to_save, TRUE)) {
            $all_entities_to_save[] = $entity_to_save;
          }
        }
      }
    }
    foreach ($all_entities_to_save as $to_save) {
      $this->save($to_save);
    }
  }
}
