<?php

namespace Drupal\prompt\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * The controller for prompt actions.
 */
class PromptController extends ControllerBase {

  /**
   * Enable the prompt.
   *
   * @param string $prompt
   *   The prompt name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function enableEntity($prompt) {
    $entity = $this->entityTypeManager()->getStorage('prompt')->load($prompt);
    $entity->set('status', TRUE);
    $entity->save();

    return $this->redirect('entity.prompt.collection');
  }

  /**
   * Disable the prompt.
   *
   * @param string $prompt
   *   The prompt name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function disableEntity($prompt) {
    $entity = $this->entityTypeManager()->getStorage('prompt')->load($prompt);
    $entity->set('status', FALSE);
    $entity->save();

    return $this->redirect('entity.prompt.collection');
  }

}
